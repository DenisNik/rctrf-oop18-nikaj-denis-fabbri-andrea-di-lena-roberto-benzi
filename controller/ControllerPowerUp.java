package controller;

import model.powerup.ChangeAppearance;
import model.powerup.Life;
import model.powerup.PowerUp;
import model.powerup.Shield;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class creates a number of PoweUP each time it is called
 * 
 * @author Federico Benzi
 */
public class ControllerPowerUp {
	private static PowerUp p;
	private static List<PowerUp> powerUpList = new ArrayList<>();

	/**
	 * Algorithm for creating PowerUP
	 *
	 * @return powerupList
	 */
	public synchronized List<PowerUp> spawnPowerUp() {
		Random random = new Random();
		powerUpList.clear();

		int powerup = random.nextInt(100);

		if (powerup < 50) { // 50%
			p = new Shield();
		} else if (powerup < 90) { // 40%
			p = new Life();
		} else { // 10%
			p = new ChangeAppearance();
		}

		powerUpList.add(p);
		return powerUpList;
	}

}
