package model;


/**
 * The entities type 
 * @author Roberto Di Lena
 *
 */
public enum EntityType {
	
	PLAYER, 
	
	CAR,
	
	TRUCK,
	
	MOTORBIKE,
	
	SHIELD,
	
	LIFE,

	APPEARANCE
}


