package model.powerup;

import model.EntityType;
import model.Player;

import java.util.Random;

public class Shield extends PowerUpImpl {
	private static Random random = new Random();

	public Shield() {
		super(EntityType.SHIELD, 100 * (random.nextInt(2) + 1), 1);
	}

	/**
	 * Manage player collision with a PowerUp
	 * 
	 * @param player which collides
	 * @return player with changed parameters
	 */
	@Override
	public Player collision(Player player) {
		player.setShield(player.getShield() + 1);
		return player;
	}
}
