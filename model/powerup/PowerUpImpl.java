package model.powerup;

import javafx.scene.shape.Rectangle;
import model.EntityType;
import utility.Pair;

/**
 * This class allows the creation of PowerUP object
 *
 * @author Federico Benzi
 */

public abstract class PowerUpImpl implements PowerUp {

	private static final double POWERUP_WIDTH = 60;
	private static final double POWERUP_HEIGHT = 60;
	private static final double WINDOWS_HEIGHT = 700;
	private static final double SPEED = 2;
	private int id;
	private Rectangle hitbox;
	private Pair<Double, Double> position = null;
	private EntityType type;

	public PowerUpImpl(final EntityType type, final double x, final int id) {
		this.type = type;
		this.id = id;
		this.hitbox = new Rectangle(POWERUP_WIDTH, POWERUP_HEIGHT);
		this.position = new Pair<>(x - (POWERUP_WIDTH / 2), 0 - POWERUP_HEIGHT);
	}

	/**
	 * update PowerUP position by speed
	 */
	public void update() {
		this.position.setY(this.position.getY() + SPEED);
		this.hitbox.setLayoutX(this.position.getX());
		this.hitbox.setLayoutY(this.position.getY());
	}

	/**
	 * @return position
	 */
	public Pair<Double, Double> getPosition() {
		return this.position;
	}

	/**
	 *
	 * @return id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * return hitbox
	 */
	public Rectangle getHitbox() {
		return this.hitbox;
	}

	/**
	 * @return type
	 */
	public EntityType getType() {
		return this.type;
	}

	/**
	 * set position out of the map
	 */
	public void setOut() {
		this.position.setY(WINDOWS_HEIGHT);
		this.hitbox.setLayoutX(this.position.getX());
		this.hitbox.setLayoutY(this.position.getY());
	}
}
