package model.powerup;

import model.Entity;
import model.Player;

public interface PowerUp extends Entity {

	/**
	 * Manage player collision with a PowerUp
	 * 
	 * @param player which collides
	 * @return player with changed parameters
	 */
	public Player collision(final Player player);

	/**
	 * set position out of the map
	 */
	public void setOut();
}
