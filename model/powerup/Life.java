package model.powerup;

import model.EntityType;
import model.Player;

import java.util.Random;

public class Life extends PowerUpImpl {
	private static final int HEALTH_BUF = 50;
	private static Random random = new Random();

	public Life() {
		super(EntityType.LIFE, 100 * (random.nextInt(2) + 1), 2);
	}

	/**
	 * Manage player collision with a PowerUp
	 * 
	 * @param player which collides
	 * @return player with changed parameters
	 */
	@Override
	public Player collision(Player player) {
		player.setHealth(player.getHealth() + HEALTH_BUF);
		return player;
	}
}
