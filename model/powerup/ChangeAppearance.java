package model.powerup;

import model.EntityType;
import model.Player;

import java.util.Random;

public class ChangeAppearance extends PowerUpImpl {
	private static Random random = new Random();

	public ChangeAppearance() {
		super(EntityType.APPEARANCE, 100 * (random.nextInt(2) + 1), 3);
	}

	/**
	 * Manage player collision with a PowerUp
	 * 
	 * @param player which collides
	 * @return player with changed parameters
	 */
	@Override
	public Player collision(Player player) {
		Random r = new Random();
		int car = r.nextInt(100);

		if (car < 90) { // 90% lambo
			player.setId(2);
		} else { // 10% tank
			player.setHealth(200);
			player.setId(3);
		}

		return player;
	}
}
